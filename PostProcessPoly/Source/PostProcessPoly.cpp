/*******************************************
	PostProcessPoly.cpp

	Main scene and game functions
********************************************/

#include <Windows.h>
#include <sstream>
#include <string>
using namespace std;

#include <d3d10.h>
#include <d3dx10.h>
#include <math.h>

#include "Defines.h"
#include "CVector3.h"
#include "CVector4.h"
#include "Camera.h"
#include "Light.h"
#include "EntityManager.h"
#include "Messenger.h"
#include "CParseLevel.h"
#include "PostProcessPoly.h"
#include <AntTweakBar.h>

namespace gen
{

TwBar* tweakBar;

//*****************************************************************************
// Post-process data
//*****************************************************************************

//Callbacks
void TW_CALL Move2Pos1(void* data);
void TW_CALL Move2Pos2(void* data);
void TW_CALL Move2Pos3(void* data);
void TW_CALL Move2Pos4(void* data);

// Enumeration of different post-processes
enum PostProcesses
{
	Copy	= 0,
	Tint	= 1,
	Blur	= 2,
	Shockwave = 3,
	Spiral	= 4,     /*GreyNoise, Burn, Distort,  HeatHaze,*/
	Edge	= 5,
	Depth	= 6,
	Bloom	= 7,
	MotionBlur = 8,
	NumPostProcesses
};

// Currently used post process
PostProcesses FullScreenFilter = Copy;

// Post-process settings
float BurnLevel = 0.0f;
const float BurnSpeed = 0.2f;
float SpiralTimer = 0.0f;
const float SpiralSpeed = 1.0f;
float HeatHazeTimer = 0.0f;
const float HeatHazeSpeed = 1.0f;
float sigma = 4.0f;
float focalPlane = 0.0f;
float focalWidth = 0.01f;

CMatrix4x4 viewProjMatrix;
CMatrix4x4 prevViewProjMatrix;

// Separate effect file for full screen & area post-processes. Not necessary to use a separate file, but convenient given the architecture of this lab
ID3D10Effect* PPEffect;

// Technique name for each post-process
const string PPTechniqueNames[NumPostProcesses] = {	"PPCopy", "PPTint", "PPBlur","PPShockWave", "PPSpiral","PPEdge","PPDepth","PPBloom","PPMotionBlur",/*"PPGreyNoise", "PPBurn", "PPDistort", "PPHeatHaze"*/ };

// Technique pointers for each post-process
ID3D10EffectTechnique* PPTechniques[NumPostProcesses];
bool enabledEffects[NumPostProcesses]; 


// Will render the scene to a texture in a first pass, then copy that texture to the back buffer in a second post-processing pass
// So need a texture and two "views": a render target view (to render into the texture - 1st pass) and a shader resource view (use the rendered texture as a normal texture - 2nd pass)
ID3D10Texture2D*          SceneTexture = NULL;
ID3D10RenderTargetView*   SceneRenderTarget = NULL;
ID3D10ShaderResourceView* SceneShaderResource = NULL;
ID3D10Texture2D*          SceneTexture2 = NULL;
ID3D10RenderTargetView*   SceneRenderTarget2 = NULL;
ID3D10ShaderResourceView* SceneShaderResource2 = NULL;

ID3D10Texture2D*          SceneTexture3 = NULL;
ID3D10ShaderResourceView* SceneShaderResource3 = NULL;
ID3D10EffectShaderResourceVariable* SceneTextureVar3 = NULL;

ID3D10ShaderResourceView* DepthShaderResource = NULL;

// Additional textures used by post-processes
ID3D10ShaderResourceView* NoiseMap = NULL;
ID3D10ShaderResourceView* BurnMap = NULL;
ID3D10ShaderResourceView* DistortMap = NULL;

// Variables to link C++ post-process textures to HLSL shader variables (for area / full-screen post-processing)
ID3D10EffectShaderResourceVariable* SceneTextureVar = NULL;
ID3D10EffectShaderResourceVariable* DepthTextureVar = NULL;
ID3D10EffectShaderResourceVariable* PostProcessMapVar = NULL; // Single shader variable used for the three maps above (noise, burn, distort). Only one is needed at a time

// Variables specifying the area used for post-processing
ID3D10EffectVectorVariable* PPAreaTopLeftVar = NULL;
ID3D10EffectVectorVariable* PPAreaBottomRightVar = NULL;
ID3D10EffectScalarVariable* PPAreaDepthVar = NULL;

// Other variables for individual post-processes
ID3D10EffectVectorVariable* TintColourVar = NULL;
ID3D10EffectVectorVariable* NoiseScaleVar = NULL;
ID3D10EffectVectorVariable* NoiseOffsetVar = NULL;
ID3D10EffectScalarVariable* DistortLevelVar = NULL;
ID3D10EffectScalarVariable* BurnLevelVar = NULL;
ID3D10EffectScalarVariable* SpiralTimerVar = NULL;
ID3D10EffectScalarVariable* HeatHazeTimerVar = NULL;

//ID3D10Texture2D*          DepthTexture2D	= NULL;
//ID3D10DepthStencilView*   DepthView			= NULL;
//ID3D10ShaderResourceView* DepthTexture		= NULL;
//ID3D10EffectShaderResourceVariable* DepthTextureVar = NULL;

D3DXVECTOR3 colourDirection = {0.0005f,0.00025f,0.00075f};
D3DXVECTOR3 colourChange = {0.5f,0.5f,0.5f};
D3DXVECTOR3 HSLcolour = {0.0f,1.0f,0.5f};

ID3D10EffectScalarVariable* shockwaveAmount = NULL;
ID3D10EffectScalarVariable* timeout = NULL;

const float TIMEOUT = 0.0f;
const float SHAKE = 0.0f;
const float WIGGLE = 15.3f;

float prevShake = 100;
float timeoutCounter = TIMEOUT;
float shake = SHAKE;
float wiggle = WIGGLE;

ID3D10EffectVectorVariable* BlurDirVar = NULL;
ID3D10EffectScalarVariable* FocalPlaneVar = NULL;
ID3D10EffectScalarVariable* FocalWidthVar = NULL;
ID3D10EffectScalarVariable* SigmaVar = NULL;
ID3D10EffectScalarVariable* TextureHeightVar = NULL;
ID3D10EffectScalarVariable* TextureWidthVar = NULL;
ID3D10EffectMatrixVariable* InverseViewVar = NULL;
ID3D10EffectMatrixVariable* PrevViewProjVar = NULL;


//*****************************************************************************


//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------

// Control speed
const float CameraRotSpeed = 2.0f;
float CameraMoveSpeed = 80.0f;

// Amount of time to pass before calculating new average update time
const float UpdateTimePeriod = 0.25f;

// Best viewing positions for the windows
const CVector3 windowPos1 = CVector3(18.25,28.5,-83.5);
const CVector3 windowPos2 = CVector3(22.75,28.5,-83.5);
const CVector3 windowPos3 = CVector3(27.25,28.5,-83.5);
const CVector3 windowPos4 = CVector3(31.85,28.5,-83.5);



//-----------------------------------------------------------------------------
// Global system variables
//-----------------------------------------------------------------------------

// Folders used for meshes/textures and effect files
extern const string MediaFolder;
extern const string ShaderFolder;

// Get reference to global DirectX variables from another source file
extern ID3D10Device*           g_pd3dDevice;
extern IDXGISwapChain*         SwapChain;
extern ID3D10DepthStencilView* DepthStencilView;
extern ID3D10Texture2D*		   DepthStencil;
extern ID3D10RenderTargetView* BackBufferRenderTarget;
extern ID3DX10Font*            OSDFont;

// Actual viewport dimensions (fullscreen or windowed)
extern TUInt32 BackBufferWidth;
extern TUInt32 BackBufferHeight;

// Current mouse position
extern TUInt32 MouseX;
extern TUInt32 MouseY;

// Messenger class for sending messages to and between entities
extern CMessenger Messenger;


//-----------------------------------------------------------------------------
// Global game/scene variables
//-----------------------------------------------------------------------------

// Entity manager and level parser
CEntityManager EntityManager;
CParseLevel LevelParser( &EntityManager );

// Other scene elements
const int NumLights = 2;
CLight*  Lights[NumLights];
CCamera* MainCamera;

// Sum of recent update times and number of times in the sum - used to calculate
// average over a given time period
float SumUpdateTimes = 0.0f;
int NumUpdateTimes = 0;
float AverageUpdateTime = -1.0f; // Invalid value at first


//-----------------------------------------------------------------------------
// Game Constants
//-----------------------------------------------------------------------------

// Lighting
const SColourRGBA AmbientColour( 0.3f, 0.3f, 0.4f, 1.0f );
CVector3 LightCentre( 0.0f, 30.0f, 50.0f );
const float LightOrbit = 170.0f;
const float LightOrbitSpeed = 0.2f;


//-----------------------------------------------------------------------------
// Scene management
//-----------------------------------------------------------------------------

// Creates the scene geometry
bool SceneSetup()
{
	TwInit(TW_DIRECT3D10,g_pd3dDevice);
	TwWindowSize(BackBufferWidth,BackBufferHeight);
	tweakBar = TwNewBar("TweakBar");

	TwAddVarRW(tweakBar,"Tint",TW_TYPE_BOOLCPP,&enabledEffects[Tint],"group=Effects");
	TwAddVarRW(tweakBar,"Blur",TW_TYPE_BOOLCPP,&enabledEffects[Blur],"group=Effects");
	TwAddVarRW(tweakBar,"Shockwave",TW_TYPE_BOOLCPP,&enabledEffects[Shockwave],"group=Effects");
	TwAddVarRW(tweakBar,"Edge Detection",TW_TYPE_BOOLCPP,&enabledEffects[Edge],"group=Effects");
	TwAddVarRW(tweakBar,"Depth",TW_TYPE_BOOLCPP,&enabledEffects[Depth],"group=Effects");
	TwAddVarRW(tweakBar,"Bloom",TW_TYPE_BOOLCPP,&enabledEffects[Bloom],"group=Effects");
	TwAddVarRW(tweakBar,"Motion Blur",TW_TYPE_BOOLCPP,&enabledEffects[MotionBlur],"group=Effects");
	//TwAddVarRW(tweakBar,"Reset",TW_TYPE_BOOLCPP,&enabledEffects[Copy],"group=Effects");

	TwAddSeparator(tweakBar,"","");

	TwAddVarRW(tweakBar,"Blur Amount",TW_TYPE_FLOAT,&sigma,"min=1 max=24 step=0.5");
	TwAddVarRW(tweakBar,"Focal Plane",TW_TYPE_FLOAT,&focalPlane,"min=0.0 max=1.0 step=0.001");
	TwAddVarRW(tweakBar,"Focal Width",TW_TYPE_FLOAT,&focalWidth,"min=0.0 max=1.0 step=0.0001");

	TwAddSeparator(tweakBar,"","");

	TwAddButton(tweakBar,"Spade",Move2Pos1,nullptr,"group=Positions");
	TwAddButton(tweakBar,"Diamond",Move2Pos2,nullptr,"group=Positions");
	TwAddButton(tweakBar,"Club",Move2Pos3,nullptr,"group=Positions");
	TwAddButton(tweakBar,"Heart",Move2Pos4,nullptr,"group=Positions");


	// Prepare render methods
	InitialiseMethods();
	
	// Read templates and entities from XML file
	if (!LevelParser.ParseFile( "Entities.xml" )) return false;
	
	// Set camera position and clip planes
	MainCamera = new CCamera( CVector3( 25, 30, -115 ));
	MainCamera->SetNearFarClip( 2.0f, 300000.0f ); 

	// Sunlight
	Lights[0] = new CLight( CVector3( -10000.0f, 6000.0f, 0000.0f), SColourRGBA(1.0f, 0.8f, 0.6f) * 12000, 20000.0f ); // Colour is multiplied by light brightness

	// Light orbiting area
	Lights[1] = new CLight( LightCentre, SColourRGBA(0.0f, 0.2f, 1.0f) * 50, 100.0f );

	return true;
}

void SetTextureDescription(D3D10_TEXTURE2D_DESC &texDesc)
{
	texDesc.Width  = BackBufferWidth;
	texDesc.Height = BackBufferHeight;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D10_USAGE_DEFAULT;
	texDesc.BindFlags = D3D10_BIND_DEPTH_STENCIL | D3D10_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
}


// Release everything in the scene
void SceneShutdown()
{
	// Release render methods
	ReleaseMethods();

	// Release lights
	for (int light = NumLights - 1; light >= 0; --light)
	{
		delete Lights[light];
	}

	// Release camera
	delete MainCamera;

	// Destroy all entities
	EntityManager.DestroyAllEntities();
	EntityManager.DestroyAllTemplates();
}


//*****************************************************************************
// Post Processing Setup
//*****************************************************************************

// Prepare resources required for the post-processing pass
bool PostProcessSetup()
{
	enabledEffects[0] = true;

	// Create the "scene texture" - the texture into which the scene will be rendered in the first pass
	D3D10_TEXTURE2D_DESC textureDesc;
	textureDesc.Width  = BackBufferWidth;  // Match views to viewport size
	textureDesc.Height = BackBufferHeight;
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // RGBA texture (8-bits each)
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D10_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	if (FAILED(g_pd3dDevice->CreateTexture2D( &textureDesc, NULL, &SceneTexture ))) return false;
	if (FAILED(g_pd3dDevice->CreateTexture2D( &textureDesc, NULL, &SceneTexture2 ))) return false;
	if (FAILED(g_pd3dDevice->CreateTexture2D( &textureDesc, NULL, &SceneTexture3 ))) return false;

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	if (FAILED(g_pd3dDevice->CreateRenderTargetView( SceneTexture, NULL, &SceneRenderTarget ))) return false;
	if (FAILED(g_pd3dDevice->CreateRenderTargetView( SceneTexture2,NULL,&SceneRenderTarget2 ))) return false;

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D10_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;

	D3D10_SHADER_RESOURCE_VIEW_DESC dpDesc;
	dpDesc.Format = DXGI_FORMAT_R32_FLOAT;
	dpDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	dpDesc.Texture2D.MostDetailedMip = 0;
	dpDesc.Texture2D.MipLevels = 1;

	if (FAILED(g_pd3dDevice->CreateShaderResourceView( SceneTexture, &srDesc, &SceneShaderResource ))) return false;
	if (FAILED(g_pd3dDevice->CreateShaderResourceView( SceneTexture2,&srDesc,&SceneShaderResource2 ))) return false;
	if (FAILED(g_pd3dDevice->CreateShaderResourceView( SceneTexture3,&srDesc,&SceneShaderResource3 ))) return false;
	if (FAILED(g_pd3dDevice->CreateShaderResourceView( DepthStencil, &dpDesc, &DepthShaderResource ))) return false;
	
	// Load post-processing support textures
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, (MediaFolder + "Noise.png").c_str() ,   NULL, NULL, &NoiseMap,   NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, (MediaFolder + "Burn.png").c_str() ,    NULL, NULL, &BurnMap,    NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, (MediaFolder + "Distort.png").c_str() , NULL, NULL, &DistortMap, NULL ) )) return false;


	// Load and compile a separate effect file for post-processes.
	ID3D10Blob* pErrors;
	DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS; // These "flags" are used to set the compiler options

	string fullFileName = ShaderFolder + "PostProcess.fx";
	if( FAILED( D3DX10CreateEffectFromFile( fullFileName.c_str(), NULL, NULL, "fx_4_0", dwShaderFlags, 0, g_pd3dDevice, NULL, NULL, &PPEffect, &pErrors, NULL ) ))
	{
		if (pErrors != 0)  MessageBox( NULL, reinterpret_cast<char*>(pErrors->GetBufferPointer()), "Error", MB_OK ); // Compiler error: display error message
		else               MessageBox( NULL, "Error loading FX file. Ensure your FX file is in the same folder as this executable.", "Error", MB_OK );  // No error message - probably file not found
		return false;
	}

	// There's an array of post-processing technique names above - get array of post-process techniques matching those names from the compiled effect file
	for (int pp = 0; pp < NumPostProcesses; pp++)
	{
		PPTechniques[pp] = PPEffect->GetTechniqueByName( PPTechniqueNames[pp].c_str() );
	}

	// Link to HLSL variables in post-process shaders
	SceneTextureVar      = PPEffect->GetVariableByName( "SceneTexture" )->AsShaderResource();
	SceneTextureVar3     = PPEffect->GetVariableByName( "SceneTexture3" )->AsShaderResource();
	DepthTextureVar		 = PPEffect->GetVariableByName( "DepthTexture" )->AsShaderResource();
	PostProcessMapVar    = PPEffect->GetVariableByName( "PostProcessMap" )->AsShaderResource();
	PPAreaTopLeftVar     = PPEffect->GetVariableByName( "PPAreaTopLeft" )->AsVector();
	PPAreaBottomRightVar = PPEffect->GetVariableByName( "PPAreaBottomRight" )->AsVector();
	PPAreaDepthVar       = PPEffect->GetVariableByName( "PPAreaDepth" )->AsScalar();
	TintColourVar        = PPEffect->GetVariableByName( "TintColour" )->AsVector();
	NoiseScaleVar        = PPEffect->GetVariableByName( "NoiseScale" )->AsVector();
	NoiseOffsetVar       = PPEffect->GetVariableByName( "NoiseOffset" )->AsVector();
	DistortLevelVar      = PPEffect->GetVariableByName( "DistortLevel" )->AsScalar();
	BurnLevelVar         = PPEffect->GetVariableByName( "BurnLevel" )->AsScalar();
	SpiralTimerVar       = PPEffect->GetVariableByName( "SpiralTimer" )->AsScalar();
	HeatHazeTimerVar     = PPEffect->GetVariableByName( "HeatHazeTimer" )->AsScalar();
	shockwaveAmount		 = PPEffect->GetVariableByName("shockwaveAmount")->AsScalar();
	timeout				 = PPEffect->GetVariableByName("timeout")->AsScalar();
	BlurDirVar			 = PPEffect->GetVariableByName("blurMultiplyVec")->AsVector();
	FocalPlaneVar		 = PPEffect->GetVariableByName("focalPlane")->AsScalar();
	FocalWidthVar		 = PPEffect->GetVariableByName("focalWidth")->AsScalar();
	SigmaVar			 = PPEffect->GetVariableByName("sigma")->AsScalar();

	TextureHeightVar	 = PPEffect->GetVariableByName("TextureHeight")->AsScalar();
	TextureWidthVar		 = PPEffect->GetVariableByName("TextureWidth")->AsScalar();
	
	InverseViewVar		 = PPEffect->GetVariableByName("inverseViewProj")->AsMatrix();
	PrevViewProjVar	 = PPEffect->GetVariableByName("prevViewProj")->AsMatrix();
	
	return true;
}

void PostProcessShutdown()
{
	if (PPEffect)            PPEffect->Release();
    if (DistortMap)          DistortMap->Release();
    if (BurnMap)             BurnMap->Release();
    if (NoiseMap)            NoiseMap->Release();
	if (SceneShaderResource) SceneShaderResource->Release();
	if (SceneRenderTarget)   SceneRenderTarget->Release();
	if (SceneTexture)        SceneTexture->Release();
}

//*****************************************************************************

inline float HUEtoRGB(float v1,float v2,float vH)
{
	if(vH < 0)
		vH += 1;

	if(vH > 1)
		vH -= 1;

	if((6 * vH) < 1)
		return (v1 + (v2 - v1) * 6 * vH);

	if((2 * vH) < 1)
		return v2;

	if((3 * vH) < 2)
		return (v1 + (v2 - v1) * ((2.0f / 3) - vH) * 6);

	return v1;
}

D3DXVECTOR3 HSLtoRGB(D3DXVECTOR3 hsl)
{
	float r = 0,g = 0,b = 0;

	if(hsl.y == 0)
	{
		r = hsl.z * 255;
		g = hsl.z * 255;
		b = hsl.z * 255;
	}
	else
	{
		float v1,v2;
		float hue = hsl.x / 360.0f;

		v2 = (hsl.z < 0.5) ? (hsl.z * (1 + hsl.y)) : ((hsl.z + hsl.y) - (hsl.z * hsl.y));
		v1 = 2 * hsl.z - v2;

		r = 255 * HUEtoRGB(v1,v2,hue + (1.0f / 3));
		g = 255 * HUEtoRGB(v1,v2,hue);
		b = 255 * HUEtoRGB(v1,v2,hue - (1.0f / 3));
	}
	
	return D3DXVECTOR3{r/100,g/100,b/100};
}

//-----------------------------------------------------------------------------
// Post Process Setup / Update
//-----------------------------------------------------------------------------

// Set up shaders for given post-processing filter (used for full screen and area processing)
void SelectPostProcess( PostProcesses filter )
{
	if(enabledEffects[Tint])
	{
		// Set the colour used to tint the scene
		D3DXCOLOR TintColour = HSLtoRGB(HSLcolour);
		TintColour.a = 0.5f;
		TintColourVar->SetRawValue( &TintColour, 0, 12 );
	}
	if(enabledEffects[Shockwave])
	{
		shockwaveAmount->SetFloat(shake);
		//timeout->SetFloat(timeoutCounter);
	}
	if(enabledEffects[Spiral])
	{
		// Set the amount of spiral - use a tweaked cos wave to animate
		SpiralTimerVar->SetFloat((1.0f - Cos(SpiralTimer)) * 4.0f);
	}

	//switch (filter)
	//{
		/*case GreyNoise:
		{
			const float GrainSize = 140; // Fineness of the noise grain

			// Set shader constants - scale and offset for noise. Scaling adjusts how fine the noise is.
			CVector2 NoiseScale = CVector2( BackBufferWidth / GrainSize, BackBufferHeight / GrainSize );
			NoiseScaleVar->SetRawValue( &NoiseScale, 0, 8 );

			// The offset is randomised to give a constantly changing noise effect (like tv static)
			CVector2 RandomUVs = CVector2( Random( 0.0f,1.0f ),Random( 0.0f,1.0f ) );
			NoiseOffsetVar->SetRawValue( &RandomUVs, 0, 8 );

			// Set noise texture
			PostProcessMapVar->SetResource( NoiseMap );
		}
		break;

		case Burn:
		{
			// Set the burn level (value from 0 to 1 during animation)
			BurnLevelVar->SetFloat( BurnLevel );

			// Set burn texture
			PostProcessMapVar->SetResource( BurnMap );
		}
		break;

		case Distort:
		{
			// Set the level of distortion
			const float DistortLevel = 0.03f;
			DistortLevelVar->SetFloat( DistortLevel );

			// Set distort texture
			PostProcessMapVar->SetResource( DistortMap );
		}
		break;
		case HeatHaze:
		{
			// Set the amount of spiral - use a tweaked cos wave to animate
			HeatHazeTimerVar->SetFloat( HeatHazeTimer );
			break;
		}*/
	//}
}

// Update post-processes (those that need updating) during scene update
void UpdatePostProcesses( float updateTime )
{
	// Not all post processes need updating
	BurnLevel = Mod( BurnLevel + BurnSpeed * updateTime, 1.0f );
	SpiralTimer   += SpiralSpeed * updateTime;
	HeatHazeTimer += HeatHazeSpeed * updateTime;
	
	HSLcolour.x += 50.0f * updateTime;
	if(HSLcolour.x > 360.0f)
		HSLcolour.x = 0.0f;

	if(enabledEffects[Shockwave])
	{
		//REDO

		timeoutCounter += updateTime;
		wiggle -= 2.0f * updateTime;

		shake -= timeoutCounter;

		shake = sinf(timeoutCounter*wiggle);

		if(fabs(prevShake - shake) <= 0.000001)
		{
			enabledEffects[Shockwave] = false;
			FullScreenFilter = Copy;
			timeoutCounter = TIMEOUT;
			wiggle = WIGGLE;
			shake = SHAKE;
			prevShake = 100;
		}
		prevShake = shake;
	}

	SigmaVar->SetFloat(sigma);
}


// Sets in the shaders the top-left, bottom-right and depth coordinates of the area post process to work on
// Requires a world point at the centre of the area, the width and height of the area (in world units), an optional depth offset (to pull or push 
// the effect of the post-processing into the scene). Also requires the camera, since we are creating a camera facing quad.
void SetPostProcessArea( CCamera* camera, CVector3 areaCentre, float width, float height, float depthOffset = 0.0f )
{
	// Get the area centre in camera space.
	CVector4 cameraSpaceCentre = CVector4(areaCentre, 1.0f) * camera->GetViewMatrix();

	// Get top-left and bottom-right of camera-facing area of required dimensions 
	cameraSpaceCentre.x -= width / 2;
	cameraSpaceCentre.y += height / 2; // Careful, y axis goes up here
	CVector4 cameraTopLeft = cameraSpaceCentre;
	cameraSpaceCentre.x += width;
	cameraSpaceCentre.y -= height;
	CVector4 cameraBottomRight = cameraSpaceCentre;

	// Get the projection space coordinates of the post process area
	CVector4 projTopLeft     = cameraTopLeft     * camera->GetProjMatrix();
	CVector4 projBottomRight = cameraBottomRight * camera->GetProjMatrix();

	// Perform perspective divide to get coordinates in normalised viewport space (-1.0 to 1.0 from left->right and bottom->top of the viewport)
	projTopLeft.x /= projTopLeft.w;
	projTopLeft.y /= projTopLeft.w;
	projBottomRight.x /= projBottomRight.w;
	projBottomRight.y /= projBottomRight.w;

	// Also do perspective divide on z to get depth buffer value for the area. Add the required depth offset (using an approximation)
	projTopLeft.z += depthOffset;
	projTopLeft.w += depthOffset;
	projTopLeft.z /= projTopLeft.w;

	// Convert the x & y coordinates to UV space (0 -> 1, y flipped). This extra step makes the shader work simpler
	projTopLeft.x =	 projTopLeft.x / 2.0f + 0.5f;
	projTopLeft.y = -projTopLeft.y / 2.0f + 0.5f;
	projBottomRight.x =	 projBottomRight.x / 2.0f + 0.5f;
	projBottomRight.y = -projBottomRight.y / 2.0f + 0.5f;

	// Send the values calculated to the shader. The post-processing vertex shader needs only these values to
	// create the vertex buffer for the quad to render, we don't need to create a vertex buffer for post-processing at all.
	PPAreaTopLeftVar->SetRawValue( &projTopLeft.Vector2(), 0, 8 );         // Viewport space x & y for top-left
	PPAreaBottomRightVar->SetRawValue( &projBottomRight.Vector2(), 0, 8 ); // Same for bottom-right
	PPAreaDepthVar->SetFloat( projTopLeft.z ); // Depth buffer value for area

	// ***NOTE*** Most applications you will see doing post-processing would continue here to create a vertex buffer in C++, and would
	// not use the unusual vertex shader that you will see in the .fx file here. That might (or might not) give a tiny performance boost,
	// but very tiny, if any (only a handful of vertices affected). I prefer this method because I find it cleaner and more flexible overall. 
}

// Set the top-left, bottom-right and depth coordinates for the area post process to work on for full-screen processing
// Since all post process code is now area-based, full-screen processing needs to explicitly set up the appropriate full-screen rectangle
void SetFullScreenPostProcessArea()
{
	CVector2 TopLeftUV     = CVector2( 0.0f, 0.0f ); // Top-left and bottom-right in UV space
	CVector2 BottomRightUV = CVector2( 1.0f, 1.0f );

	PPAreaTopLeftVar->SetRawValue( &TopLeftUV, 0, 8 );
	PPAreaBottomRightVar->SetRawValue( &BottomRightUV, 0, 8 );
	PPAreaDepthVar->SetFloat( 0.0f ); // Full screen depth set at 0 - in front of everything
}


//-----------------------------------------------------------------------------
// Game loop functions
//-----------------------------------------------------------------------------

// Draw one frame of the scene
void RenderScene()
{
	// Setup the viewport - defines which part of the back-buffer we will render to (usually all of it)
	D3D10_VIEWPORT vp;
	vp.Width  = BackBufferWidth;
	vp.Height = BackBufferHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );


	//------------------------------------------------
	// SCENE RENDER PASS - rendering to a texture
	prevViewProjMatrix = MainCamera->GetViewProjMatrix();

	// Prepare camera
	MainCamera->SetAspect( static_cast<TFloat32>(BackBufferWidth) / BackBufferHeight );
	MainCamera->CalculateMatrices();
	MainCamera->CalculateFrustrumPlanes();

	viewProjMatrix = MainCamera->GetViewProjMatrix();

	// Set camera and light data in shaders
	SetCamera( MainCamera );
	SetAmbientLight( AmbientColour );
	SetLights( &Lights[0] );

	// Specify that we will render to the scene texture in this first pass (rather than the backbuffer), will share the depth/stencil buffer with the backbuffer though
	g_pd3dDevice->OMSetRenderTargets(1,&SceneRenderTarget,DepthStencilView);

	// Clear the texture and the depth buffer
	g_pd3dDevice->ClearRenderTargetView(SceneRenderTarget,&AmbientColour.r);
	g_pd3dDevice->ClearDepthStencilView(DepthStencilView,D3D10_CLEAR_DEPTH,1.0f,0);

	g_pd3dDevice->IASetInputLayout(NULL);
	g_pd3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Render entities
	EntityManager.RenderAllEntities( MainCamera );

	
	//------------------------------------------------
	
	//------------------------------------------------
	// FULL SCREEN POST PROCESS RENDER PASS - Render full screen quad on the back-buffer mapped with the scene texture, with post-processing

	// Select the back buffer to use for rendering (will ignore depth-buffer for full-screen quad) and select scene texture for use in shader
	g_pd3dDevice->OMSetRenderTargets( 1, &SceneRenderTarget2, nullptr ); // No need to clear the back-buffer, we're going to overwrite it all
	g_pd3dDevice->ClearRenderTargetView(SceneRenderTarget2,&AmbientColour.r);

	g_pd3dDevice->CopyResource(SceneTexture2,SceneTexture);
	g_pd3dDevice->CopyResource(SceneTexture3,SceneTexture);
	
	SceneTextureVar->SetResource( SceneShaderResource );
	SceneTextureVar3->SetResource(SceneShaderResource3);
	DepthTextureVar->SetResource( DepthShaderResource );
	TextureHeightVar->SetFloat(BackBufferHeight);
	TextureWidthVar->SetFloat(BackBufferWidth);
	FocalPlaneVar->SetFloat(focalPlane);
	FocalWidthVar->SetFloat(focalWidth);

	viewProjMatrix.Invert();

	PrevViewProjVar->SetMatrix(&prevViewProjMatrix.e00);
	InverseViewVar->SetMatrix(&viewProjMatrix.e00);

	for(int i = 0;i<NumPostProcesses;++i)
	{
		if(enabledEffects[i])
		{
			SelectPostProcess((PostProcesses)i);
			SetFullScreenPostProcessArea();

			g_pd3dDevice->IASetInputLayout(NULL);
			g_pd3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			BlurDirVar->SetFloatVector(D3DXVECTOR2{1.0,0.0});
			PPTechniques[i]->GetPassByIndex(0)->Apply(0);
			g_pd3dDevice->Draw(4,0);
			g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);

			if(i == Blur)
			{
				BlurDirVar->SetFloatVector(D3DXVECTOR2{0.0,1.0});
				PPTechniques[i]->GetPassByIndex(1)->Apply(0);
				g_pd3dDevice->Draw(4,0);
				g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);
			}
			if(i == Bloom)
			{
				BlurDirVar->SetFloatVector(D3DXVECTOR2{1.0,0.0});
				PPTechniques[i]->GetPassByIndex(1)->Apply(0);
				g_pd3dDevice->Draw(4,0);
				g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);

				BlurDirVar->SetFloatVector(D3DXVECTOR2{0.0,1.0});
				PPTechniques[i]->GetPassByIndex(2)->Apply(0);
				g_pd3dDevice->Draw(4,0);
				g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);

				PPTechniques[i]->GetPassByIndex(3)->Apply(0);
				g_pd3dDevice->Draw(4,0);
				g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);
			}
			g_pd3dDevice->CopyResource(SceneTexture,SceneTexture2);
		}
	}

	SetFullScreenPostProcessArea();

	g_pd3dDevice->OMSetRenderTargets(1,&BackBufferRenderTarget,DepthStencilView); // No need to clear the back-buffer, we're going to overwrite it all
	g_pd3dDevice->ClearRenderTargetView(BackBufferRenderTarget,&AmbientColour.r);

	SceneTextureVar->SetResource(SceneShaderResource);

	g_pd3dDevice->IASetInputLayout(NULL);
	g_pd3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	PPTechniques[FullScreenFilter]->GetPassByIndex(0)->Apply(0);
	g_pd3dDevice->Draw(4,0);

	//------------------------------------------------


	//**|PPPOLY|***************************************
	// POLY POST PROCESS RENDER PASS
	// The scene has been rendered in full into a texture then copied to the back-buffer. However, the post-processed polygons were missed out. Now render the entities
	// again, but only the post-processed materials. These are rendered to the back-buffer in the correct places in the scene, but most importantly their shaders will
	// have the scene texture available to them. So these polygons can distort or affect the scene behind them (e.g. distortion through cut glass). Note that this also
	// means we can do blending (additive, multiplicative etc.) in the shader. The post-processed materials are identified with a boolean (RenderMethod.cpp). This entire
	// approach works even better with "bucket" rendering, where post-process shaders are held in a separate bucket - making it unnecessary to "RenderAllEntities" as 
	// we are doing here.

	// NOTE: Post-processing - need to set the back buffer as a render target. Relying on the fact that the section above already did that
	// Polygon post-processing occurs in the scene rendering code (RenderMethod.cpp) - so pass over the scene texture and viewport dimensions for the scene post-processing materials/shaders
	SetSceneTexture( SceneShaderResource, BackBufferWidth, BackBufferHeight );

	// Render all entities again, but flag that we only want the post-processed polygons
	EntityManager.RenderAllEntities( MainCamera, true );

	//************************************************


	//------------------------------------------------
	// AREA POST PROCESS RENDER PASS - Render smaller quad on the back-buffer mapped with a matching area of the scene texture, with different post-processing

	// NOTE: Post-processing - need to render to the back buffer and select scene texture for use in shader. Relying on the fact that the section above already did that

	// Will have post-processed area over the moving cube
	CEntity* temp = EntityManager.GetEntity( "Cubey" );

	// Set the area size, 20 units wide and high, 0 depth offset. This sets up a viewport space quad for the post-process to work on
	// Note that the function needs the camera to turn the cube's point into a camera facing rectangular area
	SetPostProcessArea( MainCamera, temp->Position(), 20, 20, -9 );

	// Select one of the post-processing techniques and render the area using it
	SelectPostProcess( Spiral ); // Make sure you also update the line below when you change the post-process method here!
	g_pd3dDevice->IASetInputLayout( NULL );
	g_pd3dDevice->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP );
	PPTechniques[Spiral]->GetPassByIndex(0)->Apply(0);
	g_pd3dDevice->Draw( 4, 0 );

	//------------------------------------------------

	// These two lines unbind the scene texture from the shader to stop DirectX issuing a warning when we try to render to it again next frame
	SceneTextureVar->SetResource( 0 );
	DepthTextureVar->SetResource(0);

	PPTechniques[FullScreenFilter]->GetPassByIndex(0)->Apply(0);

	// Render UI elements last - don't want them post-processed
	RenderSceneText();

	TwDraw();

	// Present the backbuffer contents to the display
	SwapChain->Present( 0, 0 );
}


// Render a single text string at the given position in the given colour, may optionally centre it
void RenderText( const string& text, int X, int Y, float r, float g, float b, bool centre = false )
{
	RECT rect;
	if (!centre)
	{
		SetRect( &rect, X, Y, 0, 0 );
		OSDFont->DrawText( NULL, text.c_str(), -1, &rect, DT_NOCLIP, D3DXCOLOR( r, g, b, 1.0f ) );
	}
	else
	{
		SetRect( &rect, X - 100, Y, X + 100, 0 );
		OSDFont->DrawText( NULL, text.c_str(), -1, &rect, DT_CENTER | DT_NOCLIP, D3DXCOLOR( r, g, b, 1.0f ) );
	}
}

// Render on-screen text each frame
void RenderSceneText()
{
	// Write FPS text string
	stringstream outText;
	if (AverageUpdateTime >= 0.0f)
	{
		outText << "Frame Time: " << AverageUpdateTime * 1000.0f << "ms" << endl << "FPS:" << 1.0f / AverageUpdateTime;
		RenderText( outText.str(), 2, 2, 0.0f, 0.0f, 0.0f );
		RenderText( outText.str(), 0, 0, 1.0f, 1.0f, 0.0f );
		outText.str("");
	}

	// Output post-process name
	outText << "Fullscreen Post-Process: ";
	switch (FullScreenFilter)
	{
	case Copy: 
		outText << "Copy";
		break;
	case Tint: 
		outText << "Tint";
		break;
	case Spiral: 
		outText << "Spiral";
		break;
	case Blur:
		outText << "Blur";
		break;
	/*case GreyNoise: 
		outText << "Grey Noise";
		break;
	case Burn: 
		outText << "Burn";
		break;
	case Distort: 
		outText << "Distort";
		break;
	case HeatHaze: 
		outText << "Heat Haze";
		break;*/
	}
	RenderText( outText.str(),  0, 32,  1.0f, 1.0f, 1.0f );
}


// Update the scene between rendering
void UpdateScene( float updateTime )
{
	// Call all entity update functions
	EntityManager.UpdateAllEntities( updateTime );

	// Update any post processes that need updates
	UpdatePostProcesses( updateTime );

	// Set camera speeds
	// Key F1 used for full screen toggle
	if (KeyHit( Key_F2 )) CameraMoveSpeed = 5.0f;
	if (KeyHit( Key_F3 )) CameraMoveSpeed = 40.0f;
	if (KeyHit( Key_F4 )) CameraMoveSpeed = 160.0f;
	if (KeyHit( Key_F5 )) CameraMoveSpeed = 640.0f;

	// Choose post-process
	if (KeyHit( Key_0 )) { FullScreenFilter = Copy; enabledEffects[Copy] = true; }
	if (KeyHit( Key_1 )) { FullScreenFilter = Tint; enabledEffects[Tint] = !enabledEffects[Tint]; }
	if (KeyHit( Key_2 )) { FullScreenFilter = Blur; enabledEffects[Blur] = !enabledEffects[Blur]; }
	if (KeyHit( Key_3 )) { FullScreenFilter = Shockwave; enabledEffects[Shockwave] = !enabledEffects[Shockwave]; }
	if( KeyHit( Key_4 )) { FullScreenFilter = Edge; enabledEffects[Edge] = !enabledEffects[Edge]; }


	/*if (KeyHit( Key_2 )) FullScreenFilter = GreyNoise;
	if (KeyHit( Key_3 )) FullScreenFilter = Burn;
	if (KeyHit( Key_4 )) FullScreenFilter = Distort;
	if (KeyHit( Key_5 )) FullScreenFilter = Spiral;
	if (KeyHit( Key_6 )) FullScreenFilter = HeatHaze;*/


	//Viewport Selection
	if(KeyHit(Key_U)) MainCamera->Position() = windowPos1;
	if(KeyHit(Key_I)) MainCamera->Position() = windowPos2;
	if(KeyHit(Key_O)) MainCamera->Position() = windowPos3;
	if(KeyHit(Key_P)) MainCamera->Position() = windowPos4;

	// Rotate cube and attach light to it
	CEntity* cubey = EntityManager.GetEntity( "Cubey" );
	cubey->Matrix().RotateX( ToRadians(53.0f) * updateTime );
	cubey->Matrix().RotateZ( ToRadians(42.0f) * updateTime );
	cubey->Matrix().RotateWorldY( ToRadians(12.0f) * updateTime );
	Lights[1]->SetPosition( cubey->Position() );
	
	// Rotate polygon post-processed entity
	CEntity* ppEntity = EntityManager.GetEntity( "PostProcessBlock" );
	ppEntity->Matrix().RotateY( ToRadians(30.0f) * updateTime );

	// Move the camera
	MainCamera->Control( Key_Up, Key_Down, Key_Left, Key_Right, Key_W, Key_S, Key_A, Key_D, 
	                     CameraMoveSpeed * updateTime, CameraRotSpeed * updateTime );

	// Accumulate update times to calculate the average over a given period
	SumUpdateTimes += updateTime;
	++NumUpdateTimes;
	if (SumUpdateTimes >= UpdateTimePeriod)
	{
		AverageUpdateTime = SumUpdateTimes / NumUpdateTimes;
		SumUpdateTimes = 0.0f;
		NumUpdateTimes = 0;
	}
}

void TW_CALL Move2Pos1(void* data)
{
	MainCamera->Position() = windowPos1;
}
void TW_CALL Move2Pos2(void* data)
{
	MainCamera->Position() = windowPos2;
}
void TW_CALL Move2Pos3(void* data)
{
	MainCamera->Position() = windowPos3;
}
void TW_CALL Move2Pos4(void* data)
{
	MainCamera->Position() = windowPos4;
}

} // namespace gen
