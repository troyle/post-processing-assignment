# Post Processing Effects #
This project was an assignment to show the use of post processing effects in DirectX using HLSL.  
The project features the following techniques:  

- Tint
- Motion Blur
- Gaussian Blur
- Depth of Field
- Bloom
- Edge Detection
- Poly Post Processing

Platform: Windows  
Languages/Technology: DirectX 10, HLSL, C++  
Work Period: 3 Weeks  
Type: University Project  

## For more info or to request access, contact tom@tom-royle.co.uk ##